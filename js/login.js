
// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.12.1/firebase-app.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

import{ getDatabase,onValue,ref,get,set,child,update,remove }
from "https://www.gstatic.com/firebasejs/9.12.1/firebase-database.js";

import{ getStorage, ref as refS,uploadBytes,getDownloadURL }
from "https://www.gstatic.com/firebasejs/9.12.1/firebase-storage.js";
// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyDzM6OgGrH_4kFhfFE4f3zxM9eIfHj1TAo",
    authDomain: "sitioweb-d3945.firebaseapp.com",
    databaseURL:"https://sitioweb-d3945-default-rtdb.firebaseio.com",
    projectId: "sitioweb-d3945",
    storageBucket: "sitioweb-d3945.appspot.com",
    messagingSenderId: "328468829612",
    appId: "1:328468829612:web:46595c3771da4c81525609"
  };

// Initialize Firebase

const app = initializeApp(firebaseConfig);
const db=getDatabase();


var login=document.getElementById('login');
var formulario=document.getElementById('formulario');

var usuario;
var contraseña;
var Contraseña;
var btnIniciar=document.getElementById('iniciar');
function ocultar(){
    

    login.style.display="none";
    formulario.style.display="block";

}

function comprobar()
{
    leerInputs();
    const dbref=ref(db);
    get(child(dbref,'usuarios/'+usuario)).then((snapshot)=>{
        if(snapshot.exists())
        {
            Contraseña=snapshot.val().Contraseña;
            verificar();
        }
        else{
            alert("Usuario O Contraseña Erroneas");
        }
    }).catch((error)=>{
        alert("Se ha encontrado un Error: "+error);
    });
}

function leerInputs()
{
    usuario=document.getElementById('usuario').value;
    contraseña=document.getElementById('contraseña').value;
}


function verificar()
{
    if(contraseña==Contraseña)
    {
        alert("Credenciales Correctas");
        ocultar();
    }
    else{
        alert("Usuario O Contraseña Erroneas");
    }
}

btnIniciar.addEventListener('click',comprobar);